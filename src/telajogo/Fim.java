package telajogo;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;



public class Fim extends JFrame implements ActionListener {

    
    private JLabel fim;
    private JButton JogarDeNovo, Sair; 
    
    public Fim(){
        inicia();
    }
    
    public void inicia(){
        setVisible(true);
        setSize(484, 504);
        setTitle("voce ganhou!");
        setLayout(null);
        setResizable(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        
        
        
        
        JogarDeNovo = new JButton("Jogar!");
        JogarDeNovo .setSize(200, 50);
        JogarDeNovo .setLocation(50, 150);
        JogarDeNovo .setForeground(new Color(255, 255, 255));
        JogarDeNovo .setBackground(new Color(13,6,72));
        JogarDeNovo .setFont(new Font("Castellar", 1, 18));
        JogarDeNovo .setBorder(javax.swing.BorderFactory.createLineBorder(new Color(00, 0 , 00), 3));
        add(JogarDeNovo );
    
        Sair = new JButton("sair!");
        Sair .setSize(200, 50);
        Sair .setLocation(50, 250);
        Sair .setForeground(new Color(255, 255, 255));
        Sair .setBackground(new Color(13,6,72));
        Sair .setFont(new Font("Castellar", 1, 18));
        Sair.setBorder(javax.swing.BorderFactory.createLineBorder(new Color(00, 0 , 00), 3));
        add(Sair );
        
        fim = new JLabel(new ImageIcon("img/fim.png"));
        fim.setSize(484,504);
        fim.setLocation(00, 00);
        add(fim);
    
        Sair.addActionListener(this);
        JogarDeNovo.addActionListener(this);

    
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == Sair) {
              new Tela();
              dispose();
            }   else if (e.getSource() == JogarDeNovo) {
              new TelaJogo();
              dispose();
            }
    }
    
}


