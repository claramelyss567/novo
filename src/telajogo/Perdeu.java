package telajogo;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;



public class Perdeu extends JFrame implements ActionListener {

    
    private JLabel perdeu;
    private JButton JogarN, S; 
    
    public Perdeu(){
        iniciar();
    }
    
    public void iniciar(){
        setVisible(true);
        setSize(432, 429);
        setTitle("você  perdeu ");
        setLayout(null);
        setResizable(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        
        
        
        JogarN = new JButton("Jogar!");
        JogarN .setSize(175, 50);
        JogarN .setLocation(30,290 );
        JogarN .setForeground(new Color(255, 255, 255));
        JogarN .setBackground(new Color(145,247,255));
        JogarN .setFont(new Font("Castellar", 1, 18));
        JogarN.setBorder(javax.swing.BorderFactory.createLineBorder(new Color(241, 56 , 186), 3));
        add(JogarN);
    
        S = new JButton("sair!");
        S.setSize(175, 50);
        S .setLocation(30, 350);
        S .setForeground(new Color(255, 255, 255));
        S .setBackground(new Color(145,247,255));
        S .setFont(new Font("Castellar", 1, 18));
        S.setBorder(javax.swing.BorderFactory.createLineBorder(new Color(241, 56 , 186), 3));
        add(S );
        
        perdeu = new JLabel(new ImageIcon("img/perdeu.png"));
        perdeu.setSize(432, 429);
        perdeu.setLocation(00,00);
        add(perdeu);
 
        S.addActionListener(this);
        JogarN.addActionListener(this);
    
    }
  @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == S) {
            new Tela();  
            dispose();
        }else if (e.getSource() == JogarN) {
            new TelaJogo();
            dispose();
        }
    }
  }  