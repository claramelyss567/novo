package telajogo;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;

public class Movimento extends Thread {

    private TelaJogo telaJogo;

    private boolean bateuBatom, bateuBlush, bateuRimel, bateuPincel;

    private int aux = 1;

    public Movimento(TelaJogo telaJogo) {
        this.telaJogo = telaJogo;

    }

    @Override
    public void run() {
        super.run();
        while (aux == 1) {

            try {
                sleep(20);
            } catch (InterruptedException ex) {
                Logger.getLogger(Movimento.class.getName()).log(Level.SEVERE, null, ex);
            }

            Random rand = new Random();
            
            int r = rand.nextInt(4);

            if (r == 0 && telaJogo.getBatom().getY() > 800) {
                telaJogo.getBatom().setLocation(rand.nextInt(667), -121);
            }
            if (r == 1 && telaJogo.getBlush().getY() > 800) {
                telaJogo.getBlush().setLocation(rand.nextInt(667), -127);
            }
            if (r == 2 && telaJogo.getRimel().getY() > 800) {
                telaJogo.getRimel().setLocation(rand.nextInt(667), -120);
            }
            if (r == 3 && telaJogo.getPincel().getY() > 800 && telaJogo.getPontos() >= 100) {
                telaJogo.getPincel().setLocation(rand.nextInt(667), -67);
            }

            if (telaJogo.getBatom().getY() <= 800) {
                telaJogo.getBatom().setLocation(telaJogo.getBatom().getX(), telaJogo.getBatom().getY() + 5);
                if (telaJogo.getBatom().getBounds().intersects(telaJogo.getEstojo().getBounds()) && bateuBatom == false) {
                    bateuBatom = true;
                    System.out.println("entrou no if 1");
                    telaJogo.getBatom().setVisible(false);
                    telaJogo.setPontos(telaJogo.getPontos() + 5);
                    telaJogo.getLabelPontos().setText("Pontos: " + telaJogo.getPontos());
                }
                if (telaJogo.getBatom().getY() > 679 && telaJogo.getBatom().isVisible() == true && bateuBatom == false) {
                    bateuBatom = true;
                    telaJogo.setPontos(telaJogo.getPontos() - 15);
                    telaJogo.getLabelPontos().setText("Pontos: " + telaJogo.getPontos());
                }
            } else {
                telaJogo.getBatom().setVisible(true);
                bateuBatom = false;
            }

            if (telaJogo.getBlush().getY() <= 800) {
                telaJogo.getBlush().setLocation(telaJogo.getBlush().getX(), telaJogo.getBlush().getY() + 5);
                if (telaJogo.getBlush().getBounds().intersects(telaJogo.getEstojo().getBounds()) && bateuBlush == false) {
                    bateuBlush = true;
                    System.out.println("entrou no if 2");
                    telaJogo.getBlush().setVisible(false);
                    telaJogo.setPontos(telaJogo.getPontos() + 5);
                    telaJogo.getLabelPontos().setText("Pontos: " + telaJogo.getPontos());
                }
                if (telaJogo.getBlush().getY() > 673 && telaJogo.getBlush().isVisible() == true && bateuBlush == false) {
                    bateuBlush = true;
                    telaJogo.setPontos(telaJogo.getPontos() - 15);
                    telaJogo.getLabelPontos().setText("Pontos: " + telaJogo.getPontos());
                }
            } else {

                telaJogo.getBlush().setVisible(true);
                bateuBlush = false;
            }

            if (telaJogo.getRimel().getY() <= 800) {
                telaJogo.getRimel().setLocation(telaJogo.getBatom().getX(), telaJogo.getRimel().getY() + 5);
                if (telaJogo.getRimel().getBounds().intersects(telaJogo.getEstojo().getBounds()) && bateuRimel == false) {
                    bateuRimel = true;
                    System.out.println("entrou no if 3");
                    telaJogo.getRimel().setVisible(false);
                    telaJogo.setPontos(telaJogo.getPontos() + 5);
                    telaJogo.getLabelPontos().setText("Pontos: " + telaJogo.getPontos());
                }
                if (telaJogo.getRimel().getY() > 680 && telaJogo.getRimel().isVisible() == true && bateuRimel == false) {
                    bateuRimel = true;
                    telaJogo.setPontos(telaJogo.getPontos() - 15);
                    telaJogo.getLabelPontos().setText("Pontos: " + telaJogo.getPontos());
                }
            } else {

                telaJogo.getRimel().setVisible(true);
                bateuRimel = false;
            }

            if (telaJogo.getPincel().getY() <= 800 && telaJogo.getPontos() >= 100) {
                telaJogo.getPincel().setLocation(telaJogo.getBatom().getX(), telaJogo.getPincel().getY() + 5);
                if (telaJogo.getPincel().getBounds().intersects(telaJogo.getEstojo().getBounds()) && bateuPincel == false) {
                    bateuPincel = true;
                    System.out.println("entrou no if pincel");
                    telaJogo.getPincel().setVisible(false);
                    telaJogo.setPontos(telaJogo.getPontos() + 10);
                    telaJogo.getLabelPontos().setText("Pontos: " + telaJogo.getPontos());
                }
                if (telaJogo.getPincel().getY() > 733 && telaJogo.getPincel().isVisible() == true && bateuPincel == false) {
                    bateuPincel = true;
                    telaJogo.setPontos(telaJogo.getPontos() - 25);
                    telaJogo.getLabelPontos().setText("Pontos: " + telaJogo.getPontos());
                }
            } else {
                telaJogo.getPincel().setLocation(00,810);
                telaJogo.getPincel().setVisible(true);
                bateuPincel = false;
            }

            if (telaJogo.getPontos() >= 200) {
                new Fim();
                System.out.println("Entrou nos 100");
                telaJogo.dispose();
                aux = 10;
            }

            if (telaJogo.getPontos() < 0) {
                new Perdeu();
                System.out.println("saiu dos 100");
                telaJogo.dispose();
                aux = 10;

            }

        }
    }
}

