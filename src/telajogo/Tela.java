package telajogo;


import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Tela extends JFrame implements ActionListener {

    private JButton iniciar, sair, sobre ;
    private JLabel fundo;
    

    public void inicializar() {
        setVisible(true);
        setSize(983, 476);
        setTitle("maquiando");
        setLayout(null);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        iniciar =new JButton("Jogar!");
        iniciar.setSize(200, 50);
        iniciar.setLocation(90, 100);
        iniciar.setForeground(new Color(255, 255, 255));
        iniciar.setBackground(new Color(255,149,184));
        iniciar.setFont(new Font("Castellar", 1, 18));
        iniciar.setBorder(javax.swing.BorderFactory.createLineBorder(new Color(249, 0 , 81), 3));
        add(iniciar);
        
        
        sair= new JButton("sair!");
        sair.setSize(200,50);
        sair.setLocation(90,200);
        sair.setForeground(new Color(255, 255, 255));
        sair.setBackground(new Color(255,149,184));
        sair.setFont(new Font("Castellar", 1, 18));
        sair.setBorder(javax.swing.BorderFactory.createLineBorder(new Color(249, 0, 81), 3));
        add(sair);

        sobre= new JButton("sobre");
        sobre.setSize(200,50);
        sobre.setLocation(90,300);
        sobre.setForeground(new Color(255, 255, 255));
        sobre.setBackground(new Color(255,149,184));
        sobre.setFont(new Font("Castellar", 1, 18));
        sobre.setBorder(javax.swing.BorderFactory.createLineBorder(new Color(249, 0, 81), 3));
        add(sobre);
        
        
        sobre.addActionListener(this);
        sair.addActionListener(this);
        iniciar.addActionListener(this);
        
        fundo = new JLabel(new ImageIcon("img/maquiagem.jpg"));
        fundo.setSize(983, 476);
        fundo.setLocation(00,00);
        add(fundo);
        
        try {   // Open an audio input stream.
                AudioInputStream audioIn = AudioSystem.getAudioInputStream(new File("audio/musica2.wav"));
                // Get a sound clip resource.
                Clip clip = AudioSystem.getClip();
                // Open audio clip and load samples from the audio input stream.
                clip.open(audioIn);
                clip.loop(Clip.LOOP_CONTINUOUSLY);
            } catch (UnsupportedAudioFileException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (LineUnavailableException e) {
                e.printStackTrace();
            }
        
        
    }

    public Tela() {
        inicializar();
        repaint();

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == sair) {
            System.exit(0);
        } else if (e.getSource() == iniciar) {
            new TelaJogo();
            dispose();
        }
         if (e.getSource() == sobre) {
             new Sobre();
       }

}
}