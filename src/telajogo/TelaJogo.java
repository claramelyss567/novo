package telajogo;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class TelaJogo extends JFrame implements KeyListener {

    private JLabel labelPontos, estojo;
    private int pontos ;
    private JLabel imagem, maquiage;
    private JLabel batom,blush,rimel,pincel;
    


    public void inicializar() {
        setVisible(true);
        setSize(800, 800);
        setTitle("boa sorte!");
        setLayout(null);
        setResizable(false);

        labelPontos = new JLabel("pontos:" + pontos);
        labelPontos.setSize(100, 50);
        labelPontos.setLocation(10, 10);
        add(labelPontos);

        estojo = new JLabel(new ImageIcon("img/estojo.jpg.png"));
        estojo.setSize(176, 152);
        estojo.setLocation(450, 620);
        add(estojo);
        
        batom = new JLabel(new ImageIcon("img/batom.png"));
        batom.setSize(128, 121);
        batom.setLocation(0, 810);
        add(batom);
        
        blush = new JLabel(new ImageIcon("img/blush.png"));
        blush.setSize(134, 127);
        blush.setLocation(134, 810);
        add(blush);
        
        rimel = new JLabel(new ImageIcon("img/rimel.png"));
        rimel.setSize(120, 120);
        rimel.setLocation(200, 810);
        add(rimel);

        pincel = new JLabel(new ImageIcon("img/pincel2.png"));
        pincel.setSize(12, 67);
        pincel.setLocation(450, 810);
        add(pincel);
        
        
        imagem = new JLabel(new ImageIcon("img/penteadeira.png"));
        imagem.setSize(800, 800);
        imagem.setLocation(00, 00);
        add(imagem); 

        

     

        addKeyListener(this);
        new Movimento(this).start();

    }

    public TelaJogo() {
        inicializar();
        
        
    }

    @Override
    public void keyTyped(KeyEvent e) {

        if (e.getKeyChar() == 'a' || e.getKeyChar() == 'A' ) {
            if (estojo.getX() > 0) {
                estojo.setLocation(estojo.getX() - 15, estojo.getY());
            }
        } else if (e.getKeyChar() == 'd' || e.getKeyChar() == 'D') {
            if (estojo.getX() + estojo.getWidth() < 800) {
                estojo.setLocation(estojo.getX() + 15, estojo.getY());
            }
        }
       
    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    public JLabel getLabelPontos() {
        return labelPontos;
    }

    public void setLabelPontos(JLabel labelPontos) {
        this.labelPontos = labelPontos;
    }

    public JLabel getEstojo() {
        return estojo;
    }

    public void setEstojo(JLabel estojo) {
        this.estojo = estojo;
    }

    public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    public JLabel getImagem() {
        return imagem;
    }

    public void setImagem(JLabel imagem) {
        this.imagem = imagem;
    }

    public JLabel getMaquiage() {
        return maquiage;
    }

    public void setMaquiage(JLabel maquiage) {
        this.maquiage = maquiage;
    }

   
    public JLabel getBatom() {
        return batom;
    }

    public void setBatom(JLabel batom) {
        this.batom = batom;
    }

    public JLabel getBlush() {
        return blush;
    }

    public void setBlush(JLabel blush) {
        this.blush = blush;
    }

    public JLabel getRimel() {
        return rimel;
    }

    public void setRimel(JLabel rimel) {
        this.rimel = rimel;
    }

    public JLabel getPincel() {
        return pincel;
    }

    public void setPincel(JLabel pincel) {
        this.pincel = pincel;
    }

   }
